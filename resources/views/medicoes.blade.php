@extends('templates.base')

@section('conteudo')
    
<main>
    <h1>Mediçoes:</h1>
    <hr>
    <h2>Valores Obtidos</h2>
        <table class="table table-striped table-bordered" id="tbDados">
            <thead>
                <tr>
                    <th>Pilha/Bateria</th>
                    <th>tensão nominal (V)</th>
                    <th>Capacidade de corrente </th>
                    <th>Tensão sem carga(V)</th>
                    <th>Tensão com carga(V)</th>
                    <th>Resistência de carga(ohm)</th>
                    <th>Resistência interna(ohm)</th>
                </tr>

            </thead>
            <tbody>
                @foreach($medicoes as $medicao)
                <tr> 
                    <td> {{$medicao->pilha_bateria}}</td>
                    <td> {{number_format($medicao->tensao_nominal,1,'.','')}}</td>
                    <td> {{$medicao->tensao_sem_carga}}</td>
                    <td> {{$medicao->tensao_com_carga}}</td>
                    <td> {{$medicao->resistencia_carga}}</td>
                    <td> {{number_format($medicao->resistencia_interna,3,'.','')}}</td>
                    
    
                </tr>
                    
                @endforeach
            </tbody>

            <tr>
                
            </tr>
        </table>

</main>
@endsection

       @section('rodape')
       <h4> Rodapé da página principal</h4>
       @endsection
 