//alert("Tabela");
var dados = [ 
  {
      pilha: "bateria Freedom 30Ah",
      tensao_nominal: 12,
      corrente: 26000,
      E: 10.68,
      V: 10.67,                         /*tensao com carga*/
      R: 23.000,
  },

  {
      pilha:"bateria Unipower 7Ah",
      tensao_nominal:12,
      corrente: 7000,
      E: 10.47,
      V: 10.38,
      R: 23.000,
  },

  {
      pilha: "Pilha Duracel AA",
      tensao_nominal: 1.5,
      corrente: 2800,
      E: 1.306,
      V: 1.291,
      R: 23.000,
  },

  {
      pilha:"Pilha golite ",
      tensao_nominal:9,
      corrente:500,
      E: 5.54,
      V: 2.49,
      R: 23.000,
  },
  {
      pilha:"Pilha panasonic AA",
      tensao_nominal:1.5,
      corrente:2500,
      E: 1.379,
      V: 1.338,
      R: 23.000,
  },
  {
      pilha:"Pilha duracell plus",
      tensao_nominal:1.5,
      corrente:1200,
      E: 0.989,
      V: 0.830,
      R: 23.000,
  },
  {
      pilha:"Pilha Luatex ",
      tensao_nominal:3.7,
      corrente:1200,
      E: 2.479,
      V: 2.347, 
      R: 23.000,
  },
  {
      pilha:"Pilha JYX",
      tensao_nominal:1.5,
      corrente:9800,
      E: 2.564,
      V: 2.236, 
      R: 23.000,
  },
  {
      pilha:"Pilha elgin",
      tensao_nominal:9,
      corrente:250,
      E: 7.11,
      V: 2.85,
      R: 23.000,
  },
  {
      pilha:"Pilha Philips",
      tensao_nominal:1.5,
      corrente:1200,
      E: 1.370,
      V: 1.307,
      R: 23.000,
  }
];
function calcularResistencia() {
  var tabela = document.getElementById("tbDados");
 
  // Limpar tabela antes de preencher
  var tabelaHTML = `<tr>
                            <th>Pilha/Bateria</th>
                            <th>Tensão nominal (V)</th>
                            <th>Capacidade de corrente (mA.h)</th>
                            <th>Tensão sem carga(V)</th>
                            <th>Tensão com carga(V)</th>
                            <th>Resitência de carga(ohm)</th>
                            <th>Resistência interna(ohm)</th>
                  </tr>`;
 
  // Iterar sobre os dados e calcular o r de cada um
  for (var i = 0; i < dados.length; i++) {
    var linha = dados[i];
    var E = linha.E;
    var V = linha.V;
    var R = linha.R;
    var r = R * (E / V - 1);
 
    // Adicionar nova linha à tabela com os valores e a soma
    var novaLinha = "<tr><td>" + linha.pilha + "</td><td>" + linha.tensao_nominal + "</td><td>" + linha.corrente + "</td><td>" + linha.E + "</td><td>" + linha.V + "</td><td>" + linha.R + "</td><td>" + r.toFixed(4) + "</td></tr>";
 
    tabelaHTML += novaLinha;
  }
  tabela.innerHTML = tabelaHTML;
}
 
calcularResistencia();